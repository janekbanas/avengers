import $ from 'jquery';
import '../node_modules/popper.js/dist/popper.min.js';
import '../node_modules/bootstrap';
import '../node_modules/simplebar/dist/simplebar';
import slick from '../node_modules/slick-carousel/slick/slick';
import Vue from '../node_modules/vue/dist/vue';
import VeeValidate from '../node_modules/vee-validate';
import axios from '../node_modules/axios';
import pl from './language/pl';
import en from './language/en';
import kor from './language/kor';
import navComponent from './components/nav';

Vue.use(VeeValidate);

new Vue({
    el: '#app',
    data: {
        formData: {
            name: '',
            email: '',
            message: '',
            zgoda1: false,
            zgoda2: false,
            zgoda3: false,
            // zgoda4: false,
            // zgoda5: false,
        },
        newsData: {
            newsEmail: '',
            newsZgoda: false,
        },
        lang: en,
        selectLang: 'pl',
        test: 'test',
        counter: {
            d: 0,
            h: 0,
            m: 0,
            s: 0
        }
    },
    created: function() {
        this.clock();

    },
    methods: {
        selectAll: function() {
            console.log(this.formData.zgoda1);

            if (this.formData.zgoda1) {
                this.formData.zgoda2 = true;
                this.formData.zgoda3 = true;
                // this.formData.zgoda4 = true;
                // this.formData.zgoda5 = true;
            } else {
                this.formData.zgoda2 = false;
                this.formData.zgoda3 = false;
                // this.formData.zgoda4 = false;
                // this.formData.zgoda5 = false;
            }
        },
        formSend: function() {

            let data = new FormData();

            data.append('name', this.formData.name);
            data.append('email', this.formData.email);
            data.append('message', this.formData.message);
            data.append('zgoda1', this.formData.zgoda1);
            data.append('zgoda2', this.formData.zgoda2);
            data.append('zgoda3', this.formData.zgoda3);

            this.$validator.validate('contactForm.*').then((valid) => {
                if (valid) {
                    axios.post('app/lead.php', data)
                        .then(function(response) {
                            $('#correct').toggleClass('d-none');
                            $('.formSend').prop('disabled', true);
                            $('.hide-input').addClass('d-none');
                            $('.hide-lebel').addClass('d-none');
                        })
                        .catch(function(error) {
                            $('#wrong').toggleClass('d-none');
                        });
                }
            });
        },
        newsSend: function() {

            let data = new FormData();

            data.append('newsEmail', this.newsData.newsEmail);
            data.append('newsZgoda', this.newsData.newsZgoda);

            this.$validator.validate('newsletterForm.*').then((valid) => {
                if (valid) {
                    axios.post('app/newsletter.php', data)
                        .then(function(response) {
                            $('.input-newsletter').addClass('d-none');
                            $('.check-newslette').addClass('d-none');
                            $('#newsCorrect').toggleClass('d-none');
                            $('.newsletterSend').prop('disabled', true);

                        })
                        .catch(function(error) {
                            $('#newsWrong').toggleClass('d-none');
                        });
                }
            });
        },
        swichLang: function(lang) {

            this.selectLang = lang;

            if (this.selectLang === 'en') {
                this.lang = en;
            }

            if (this.selectLang === 'pl') {
                this.lang = pl;
            }

            if (this.selectLang === 'kor') {
                this.lang = kor;
            }
        },
        clock: function() {

            let $this = this;

            // Set the date we're counting down to
            let countDownDate = new Date("Oct 30 2020 12:00:00").getTime();

            // Update the count down every 1 second
            let x = setInterval(function() {

                // Get today's date and time
                let now = new Date().getTime();

                // Find the distance between now and the count down date
                let distance = countDownDate - now;

                // Time calculations for days, hours, minutes and seconds
                $this.counter.d = Math.floor(distance / (1000 * 60 * 60 * 24));
                $this.counter.h = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                $this.counter.m = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
                $this.counter.s = Math.floor((distance % (1000 * 60)) / 1000);

                // If the count down is finished, write some text 
                if (distance < 0) {
                    clearInterval(x);
                    // document.getElementById("counter").innerHTML = "0";
                }
            }, 1000);
        }
    },

})



$(function() {
    let nav = $(".core-nav");
    let btn = $(".menu-btn");
    let choice = $(".lang-choice");
    $(window).scroll(function() {
        let scroll = $(window).scrollTop();

        if (scroll >= 50) {
            nav.addClass("fixed");
            btn.addClass("btn-fix");
            choice.addClass("btn-fix");

        } else {
            nav.removeClass("fixed");
            btn.removeClass("btn-fix");
            choice.removeClass("btn-fix");
        }
    });
});



$('.collapse').on('show.bs.collapse', function() {
    $(this).parent().find('.show-less').removeClass('d-none');
    $(this).parent().find('.show-more').addClass('d-none');
})

$('.collapse').on('hide.bs.collapse', function() {
    $(this).parent().find('.show-less').addClass('d-none');
    $(this).parent().find('.show-more').removeClass('d-none');
})



$('#check-more-1').click(function() {
    $('#checkbox-text-1').removeClass('less-text');
    $('#checkbox-text-1').addClass('more-text');
});
$('#check-less-1').click(function() {
    $('#checkbox-text-1').removeClass('more-text');
    $('#checkbox-text-1').addClass('less-text');
});

$('#check-more-2').click(function() {
    $('#checkbox-text-2').removeClass('less-text');
    $('#checkbox-text-2').addClass('more-text');
});
$('#check-less-2').click(function() {
    $('#checkbox-text-2').removeClass('more-text');
    $('#checkbox-text-2').addClass('less-text');
});


$('#videoModalWhite').on('hidden.bs.modal', function() {
    $("#videoModalWhite iframe").attr("src", $("#videoModalWhite iframe").attr("src"));
});
$('#videoModalToken').on('hidden.bs.modal', function() {
    $("#videoModalToken iframe").attr("src", $("#videoModalToken iframe").attr("src"));
});


$('.up-top').on("click", function() {
    $('html, body').animate({ scrollTop: 0 }, 'slow');
    return false;
});

$('ul.nav').find('a').click(function() {
    let $href = $(this).attr('href');
    let $anchor = $('#' + $href).offset();
    $('body').animate({ scrollTop: $anchor.top }, 'slow');
    return false;
});



navComponent();
export default {

    menuItem1: "Start",

    menuItem2: "English",

    menuItem3: "Team",

    menuItem4: "Roadmap",

    menuItem5: "Lorem",

    menuItem6: "FAQ",

    menuItem7: "Contact",



    menuBtn1: "Lorem ipsum",



    lang: "EN",



    pageTitle1: "Sunt explicabo",

    pageTitle2: "Nostrum exercitationem",



    pageSubtitle: "Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur quo voluptas.",



    clockStarter: "LOREM illum qui:",



    clockD: "qui",

    clockH: "volupta",

    clockMin: "illum",

    clockSec: "euman",



    minGoal: "min. lorem",

    hardCap: "hard mreol",



    tokenNumb: "dolorem",



    platformAv: "Nam libero tempore <br>10.09.2020 r.",



    btnBuyToken: "hicar alias",

    btnPreSel: "rerum hic",



    btnDownloadWhitepaper: "autem quibusdam",

    btnDownloadOnepager: "autem tempore",



    whitepaperText: "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Sed ut perspiciatis unde omnis iste natus error sit , totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae.",





    roadmapQ1Col1: "Sed ut perspiciatis unde omnis iste natus error sit ",

    roadmapQ1Col2: "Sed ut perspiciatis unde omnis iste natus error sit ",

    roadmapQ1Col3: "Sed ut perspiciatis unde omnis iste natus error sit a",



    roadmapQ2Col1: "Sed ut perspiciatis unde omnis iste natus error sit ",

    roadmapQ2Col2: "Sed ut perspiciatis unde omnis iste natus error sit ",

    roadmapQ2Col3: "Sed ut perspiciatis unde omnis iste natus error sit ",

    roadmapQ2Col4: "Sed ut perspiciatis unde omnis iste natus error sit ",

    roadmapQ2Col5: "Sed ut perspiciatis unde omnis iste natus error sit ",

    roadmapQ2Col6: "Sed ut perspiciatis unde omnis iste natus error sit ",

    roadmapQ2Col7: "Sed ut perspiciatis unde omnis iste natus error sit ",

    roadmapQ2Col8: "Sed ut perspiciatis unde omnis iste natus error sit ",

    roadmapQ2Col9: "Sed ut perspiciatis unde omnis iste natus error sit ",





    roadmapQ3Col1: "Sed ut perspiciatis unde omnis iste natus error sit ",

    roadmapQ3Col2: "Sed ut perspiciatis unde omnis iste natus error sit ",

    roadmapQ3Col3: "Sed ut perspiciatis unde omnis iste natus error sit ",

    roadmapQ3Col4: "Sed ut perspiciatis unde omnis iste natus error sit ",

    roadmapQ3Col5: "Sed ut perspiciatis unde omnis iste natus error sit ",

    roadmapQ3Col6: "Sed ut perspiciatis unde omnis iste natus error sit ",



    roadmapQ4Col1: "Sed ut perspiciatis unde omnis iste natus error sit ",

    roadmapQ4Col2: "Sed ut perspiciatis unde omnis iste natus error sit ",

    roadmapQ4Col3: "Sed ut perspiciatis unde omnis iste natus error sit ",

    roadmapQ4Col4: "Sed ut perspiciatis unde omnis iste natus error sit ",



    roadmapQ5Col1: "Sed ut perspiciatis unde omnis iste natus error sit ",

    roadmapQ5Col2: "Sed ut perspiciatis unde omnis iste natus error sit ",



    team: "OFFICA",
    teamMore: "maxime",


    team1Sector: "Itaque",


    team1Position: "Character",

    team1Name: "steve rogers",

    team1Surname: "Capitan America",

    team1Descritpion: "At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.",



    team2Position: "Character",

    team2Name: "steve rogers",

    team2Surname: "Capitan America",

    team2Descritpion: "At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.",



    team3Position: "Character",

    team3Name: "steve rogers",

    team3Surname: "Capitan America",

    team3Descritpion: "At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.",


    products: "Lorem",



    singleProdTitle1: "Aut viam inveniam aut faciam",

    singleProdDes1: "Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.",



    singleProdTitle2: "Aut viam inveniam aut faciam",

    singleProdDes2: "Omnis voluptas assumenda est, omnis dolor repellendus.<br><br> At vero eos et accusamus et iusto <span class='color-br f-bold'>odio dignissimo </span>.<br><br> ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis <span class='color-br f-bold'>doloribus asperiores repellat.</span> Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates.",



    singleProdTitle3: "Aut viam inveniam aut faciam",

    singleProdDes3: "Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.<br><br> At vero eos et accusamus et iusto <span class='color-br f-bold'>odio dignissimo </span>.<br><br> ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis <span class='color-br f-bold'>doloribus asperiores repellat.</span>",


    tokenTitle: "AIORES",



    faqTitle: "Temporibus autem quibu – FAQ",



    faqHead1: "Nam libero tempore, cum soluta?",

    faqText1: "At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis",



    faqHead2: "At vero eos et accusamus et?",

    faqText2: "At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis",



    faqHead3: "Et harum quidem rerum?",

    faqText3: "At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis",



    faqHead4: "Nam libero tempore, cum soluta nobis est eligendi?",

    faqText4: "At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi.",



    faqHead5: "Nam libero tempore, cum soluta nobis est eligendi?",

    faqText5: "At vero eos et accusamus et.",



    faqHead6: "At vero eos et accusamus et?",

    faqText6: "At vero eos et accusamus et.",



    contact: "Lorem",

    contactTitle: "Aut viam inveniam.",

    contactSubtitle: "Accusamus et iusto odio dignissimos.",



    street: "ul. T. Starka 3000 ",

    town: "00-001 New York",

    country: "USA",



    companyInfoName: "Avengers Sp. z o.o.",



    companyInfoPart: "<span class='color-br'>Avengers</span> (private limited company) accusamus et iusto odio dignissimos accusamus et iusto odio dignissimos accusamus et iusto odio dignissimos.",





    inputName: "Name",

    inputMail: "E-mail",

    inputPhone: "Phone",

    inputMessage: "Message",



    inputError: "Required.",



    checkboxAll: "I know them all.",



    checkboxTitle1: "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",

    checkboxTextMore1: "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.",



    checkboxTitle2: "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",

    checkboxTextMore2: "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.",



    checkboxTitle3: "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",

    checkboxTextMore3: "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.",



    checkboxTitle4: "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",

    checkboxTextMore4: "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.",



    checkboxReadMore: "unde omnis iste",

    checkboxReadLess: "ipsam",



    btnSend: "VELIT",



    correct: "Neque porro quisquam est.",

    wrong: "Sed ut perspiciatis dolores unde.",



    newsletterTitle: "AUT VIAM INVENIA",

    newsletterSubtitle: "Neque porro quisquam est.",



    newsletterInputEmail: "Viam e-mail",



    newsletterCheckbox: "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam ut perspiciatis unde omnis.",



    footerPolitic: "Perspiciatis Under.",



}
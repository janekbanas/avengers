<?php

include 'Validate.php';

$lead = new Lead();

class Lead{
	private $data;
	private $config;

	public function __construct()
	{
		$this->data = $_POST;

		if( $this->validate() ){
			if($this->lead()){
				$this->msg('success', 'Wiadomośc wysłana poprawnie');

			}else{
				$this->msg('error', 'Błąd wysyłania wiadomości');
			}
		}
	}

	private function lead() {
		$imie = $this->data['name'];
		$email = $this->data['email'];
		$message = $this->data['message'];
		$zgoda1 = $this->data['zgoda2'] ? 'lorem' : '';
		$zgoda2 = $this->data['zgoda3'] ? 'lorem lorem' : '';

		$body = <<<EOD
			<h3>Kontakt ze strony Avengers</h3>
			<p>
				<strong>Name:</strong> $imie<br>
				<strong>Email:</strong> $email<br>
				<strong>Message:</strong> $message<br>
				$zgoda1
				$zgoda2
			</p>
EOD;

		return $this->sendMail($this->data['email'], $this->data['name'], $body);
		
	}


	private function sendMail($email, $name, $message)
	{
		
		$data = [
			'token' => '123',
			'to' => 'contact@test.com',
			'subject' => 'Kontakt ze strony Avenger',
			'message' => $message
		];

		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, '/my/mail-api/');
		curl_setopt($curl, CURLOPT_POST, count($data));
		curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, TRUE);

		$result = curl_exec($curl);

		$result = json_decode($result, true);

		if($result['status'] === 'success'){
			return true;
		}else{
			return false;
		}

	}

	private function msg($status, $message)
	{
		header('Content-Type: application/json');
		echo json_encode(['status' => $status, 'message' => $message]);
		exit;
	}

	private function validate()
	{
		$validate = new Validate;

		  if( !$validate->name($this->data['name']) )
			   $this->msg('error', 'podaj poprawne imię i nazwisko');

		  if( !$validate->email($this->data['email']) )
			   $this->msg('error', 'Podaj porawny email');

		  // if( !$validate->phone($this->data['phone']) )
		  //      $this->msg('error', 'Podaj porawny numer telefonu');

		  if( !$validate->length($this->data['message'], ['min' => 3]) )
			   $this->msg('error', 'Wpisz wiadomości');

		  if( !isset( $this->data['zgoda2'] ) )
			   $this->msg('error', 'Zaznacz zgodę');

		  if( !isset( $this->data['zgoda3'] ) )
			   $this->msg('error', 'Zaznacz zgodę');
		  

		return true;
	}
}
<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;


include 'vendor/autoload.php';
include 'Validate.php';

$lead = new Lead();

//print_r($_POST);

class Lead{
    private $data;
    private $config;

    public function __construct()
    {
         $this->config = [
                // Host poczty SMTP
              'host' => 'smtp',

               // Port poczty SMTP
               'port' => 587,

               // Użytkownik poczty SMTP
               'username' => 'biuro@test.pl',

               // Hasło użytkownika poczty SMTP
               'password' => 'test',

               // Tutuł przesłanej wiadomości
               'mail_description' => 'Zapisanie się do newslettera Avengers',

               // Lista emaili do których ma zostać wysłany email. Po przecinkach można dodać kolejnych
               'mail_to' => [
                  'contact@test.com',
               ]
         ];
        $this->data = $_POST;


        if( $this->validate() ){
            if($this->lead()){
                $this->msg('success', 'Wiadomośc wysłana poprawnie');

            }else{
                $this->msg('error', 'Błąd wysyłania wiadomości');
            }
        }
    }

    private function lead() {
        $email = $this->data['newsEmail'];
        $zgoda = $this->data['newsZgoda'] ? 'test' : '';

        $body = <<<EOD
            <h3>Zapisanie się do newslettera Avengers</h3>
            <p>
                <strong>Email:</strong> $email<br>
                $zgoda
            </p>
EOD;

    return $this->sendMail($this->data['newsEmail'], $this->data['newsZgoda'], $body);
		
    }


    private function sendMail($email, $name, $message)
    {
    	$mail = new PHPMailer(true);                              // Passing `true` enables exceptions
		try {
		    //Server settings
        $mail->CharSet = 'UTF-8';
		    $mail->SMTPDebug = 0;                                 // Enable verbose debug output
		    // $mail->isSMTP();                                      // Set mailer to use SMTP
		    $mail->Host = $this->config['host'];  // Specify main and backup SMTP servers
		    $mail->SMTPAuth = true;                               // Enable SMTP authentication
		    $mail->Username = $this->config['username'];                 // SMTP username
		    $mail->Password = $this->config['password'];                           // SMTP password
		    $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
		    $mail->Port = $this->config['port'];                                    // TCP port to connect to

		    //Recipients
		    $mail->setFrom($this->config['username']);

		    foreach($this->config['mail_to'] as $user){
		    	$mail->addAddress($user);
		    }
		    
		    $mail->addReplyTo($email, $name);

		    //Content
		    $mail->isHTML(true);                                  // Set email format to HTML
		    $mail->Subject = 'Zapisanie się do newslettera Avengers';
		    $mail->Body    = $message;

		    $mail->send();
		    return true;
		} catch (Exception $e) {
		    return false;
		}
    }

    private function msg($status, $message)
    {
        header('Content-Type: application/json');
        echo json_encode(['status' => $status, 'message' => $message]);
        exit;
    }

    private function validate()
    {
        $validate = new Validate;

          if( !$validate->email($this->data['newsEmail']) )
               $this->msg('error', 'Podaj porawny email');

          if( !isset( $this->data['newsZgoda'] ) )
               $this->msg('error', 'Zaznacz zgodę');

        return true;
    }
}